// getters are functions
export const foo = (state, getters) => {
    return state.foo
}

export const selectedNode = (state, getters) => {
    return state.selectedNode;
}

export const buttonNewChangeClicked = (state, getters) => {
    return state.buttonNewChangeClicked;
}

export const dataChanged = (state, getters) => {
    return state.dataChanged;
}
