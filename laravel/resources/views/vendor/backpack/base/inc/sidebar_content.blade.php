<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-question'></i> Tags</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('slug') }}'><i class='nav-icon la la-question'></i> Slugs</a></li> --}}

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('change') }}'><i class='nav-icon la la-list'></i> Changes</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('change2') }}'><i class='nav-icon la la-sitemap'></i> Changes Tree</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('factory') }}'><i class='nav-icon la la-industry'></i> Factories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('unit') }}'><i class='nav-icon la la-stream'></i> Units</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('system') }}'><i class='nav-icon la la-stream'></i> Systems</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('change-status') }}'><i class='nav-icon la la-tag'></i> Change Statuses</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon la la-users'></i> Users</a></li>

{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attachment') }}'><i class='nav-icon la la-question'></i> Attachments</a></li> --}}

{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('comment') }}'><i class='nav-icon la la-question'></i> Comments</a></li> --}}
