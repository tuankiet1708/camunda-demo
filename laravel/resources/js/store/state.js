// root state object.
// each Vuex instance is just a single state tree.
const state = {
    foo: 'bar',
}

export default state;
